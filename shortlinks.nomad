job "shortlinks" {
  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }

  datacenters = ["solar-do"]
  type = "service"

  group "web" {
    count = 1

    task "server" {
      driver = "docker"

      config {
        image = "registry.gitlab.com/coala/solar/shortlinks"
      }

      service {
        address_mode = "driver"
        port = 80

        tags = [
          "traefik.enable=true",
          "traefik.frontend.rule=Host:coala.io,www.coala.io",
          "traefik.frontend.headers.STSSeconds=315569260",
          "traefik.frontend.headers.STSPreload=true",
          "traefik.frontend.headers.frameDeny=true",
          "traefik.frontend.headers.browserXSSFilter=true"
        ]
      }
    }
  }
}
